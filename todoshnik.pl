#! /usr/bin/env perl

use strict;
use warnings;

# disable "given/when is experimental" warning
no if $] >= 5.018, warnings => "experimental::smartmatch";

use 5.010;
use Getopt::Long;
use File::Find;
use File::Basename;
use Term::ANSIColor;

my $help;
my $stat;
GetOptions("h|help|?" =>\$help, "s|statistics"=>\$stat) or exit 1;
if($help)
{
	&say_help();
}

if(scalar @ARGV == 0)
{
	@ARGV=('.');
}

# colorize output only when outputting to terminal
my $with_color=0;
if(-t STDOUT)
{
    $with_color=1;
}

my $coment_key=0;
my $multi=0;
my $buf='';
my $file_count=0;
my $item_count=0;
my $full_template;
my $end_multi;
my $start_multi;
find(\&process,@ARGV);
if($stat)
{
	say "\nFound $item_count items in $file_count files";
}
#############################################

sub process
{
	if (not &check_name($_))
	{
		return;
	}

	++$file_count;

	my $fname=$File::Find::name;

	my $out='';
	open IN,$_;
	while (<IN>)
	{
		chomp;
		if($multi)
		{
			$out.=&multiline($_);
		}
		if(/$full_template/)
		{
			if($& eq "$start_multi")
			{
				$coment_key=2;
			}
			elsif($& eq $end_multi)
			{
				$coment_key=0;
			}
			else
			{
				$coment_key=1;
			}
		}
		if($coment_key>0 && /(TODO|HACK|XXX|FIXME|FIXIT):?/o)
		{
			++$item_count;

			$out.=&format("$fname:$.", $1);

			if($coment_key==2)
			{
				$multi=1;
				$out.=&multiline($');
			}
			else
			{
				$out.="$'\n";
				$coment_key=0;
			}
		}
	}
	print $out;
	close IN;
}

sub format
{
	my $location=shift;
	my $category=shift;

	if(!$with_color)
	{
		return "$location: ($category)";
	}

	my $str=colored($location, 'bold yellow').': ';
	if($category eq 'TODO')
	{
		$str.=colored("($category)", 'bold green');
	}
	else
	{
		$str.=colored("($category)", 'bold red');
	}
	return $str;
}

sub multiline
{
	my $tail=shift;
	if($tail=~/(?:\s*\*\s*)?(.*)\Q$end_multi\E/)
	{
		$multi=0;
		$coment_key=0;
		$buf="$1\n";
	}
	else
	{
		$tail=~s/^\s*\*\s*//;
		$buf=$tail;
	}
	return $buf;
}

sub check_name
{
	my $name=shift;
	given($name)
	{
		when(/\.(c|cpp|h|hpp|cxx|hxx)$/io)
		{
			$full_template = '\/\/|\/\*|\*\/';
			$end_multi ='*/';
			$start_multi ='/*';
			return 1;
		}
		when (/\.php$/io)
		{
			$full_template = '\/\/|\/\*|\*\/|#';
			$end_multi ='*/';
			$start_multi ='/*';
			return 1;
		}
		when (/\.(pm|pl)$/io)
		{
			$full_template = "#";
			$end_multi ="";
			$start_multi ="";
			return 1;
		}
		default
		{
			return 0;
		}
	}
}

sub say_help
{
	say "@{[basename($0)]} [path...]
By default current directory with all its subdirectories is processed.
Options:
	-h, --help			brief help message
	-s, --statistics	print statistics";
	exit 1;
}

# vim: noexpandtab :
